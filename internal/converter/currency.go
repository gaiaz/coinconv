package converter

import "strings"

//go:generate stringer -type=Currency
type Currency uint

const (
	InvalidCurrency Currency = iota
	USD                      // United States Dollar
	BTC                      // Bitcoin
	ETH                      // Ethereum
	BNB                      // Binance Coin
)

func ParseCurrency(s string) (Currency, bool) {
	if s == "" {
		return InvalidCurrency, false
	}
	s = strings.ToUpper(s)
	for i := 1; i < len(_Currency_index); i++ {
		li, ri := _Currency_index[i], _Currency_index[i+1]
		if s == _Currency_name[li:ri] {
			return Currency(i), true
		}
	}
	return InvalidCurrency, false
}
