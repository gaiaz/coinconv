package converter

import (
	"context"
	"fmt"

	"github.com/shopspring/decimal"
)

type Client interface {
	ConvertAmount(ctx context.Context, amount decimal.Decimal, fromCurrency, toCurrency string) (float64, error)
}

type Service struct {
	client Client
}

// New creates a new CoinMarketCap Service
func New(client Client) Service {
	return Service{client: client}
}

// ConvertAmount converts amount from first currency to second
func (s Service) ConvertAmount(ctx context.Context, amount decimal.Decimal, fromCurrency, toCurrency Currency) (decimal.Decimal, error) {
	if amount.IsZero() {
		return decimal.Zero, nil
	}

	if fromCurrency == toCurrency {
		return amount, nil
	}

	price, err := s.client.ConvertAmount(ctx, amount, fromCurrency.String(), toCurrency.String())
	if err != nil {
		return decimal.Zero, fmt.Errorf("client.ConvertAmount(): %w", err)
	}

	return decimal.NewFromFloat(price), nil
}
