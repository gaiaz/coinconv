package coinmarketcap

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"

	"github.com/shopspring/decimal"
)

type Client struct {
	httpClient *http.Client
}

func New(httpClient *http.Client) Client {
	return Client{httpClient: httpClient}
}

const (
	host   = "sandbox-api.coinmarketcap.com"
	apiKey = "b54bcf4d-1bca-4e8e-9a24-22ff2c3d462c"
)

func (c Client) ConvertAmount(ctx context.Context, amount decimal.Decimal, fromCurrency, toCurrency string) (float64, error) {
	ctx, cancel := context.WithTimeout(ctx, 2*time.Second)
	defer cancel()

	const path = "/v1/tools/price-conversion"
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, "https://"+host+path, nil)
	if err != nil {
		return 0, err
	}

	q := make(url.Values, 3)
	q.Add("amount", amount.String())
	q.Add("symbol", fromCurrency)
	q.Add("convert", toCurrency)
	req.URL.RawQuery = q.Encode()

	req.Header.Set("Accepts", "application/json")
	req.Header.Add("X-CMC_PRO_API_KEY", apiKey)

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return 0, fmt.Errorf("httpClient.Do(): %w", err)
	}
	defer resp.Body.Close()

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return 0, fmt.Errorf("ioutil.ReadAll(): %w", err)
	}

	respData := new(contract)
	err = json.Unmarshal(respBody, respData)
	if err != nil {
		return 0, fmt.Errorf("json.Unmarshal(): %w", err)
	}

	err = respData.validate()
	if err != nil {
		return 0, fmt.Errorf("respData.validate(): %w", err)
	}

	price := respData.getPrice(toCurrency)
	if price == 0 {
		return 0, errors.New("respData.getPrice()")
	}

	return price, nil
}
