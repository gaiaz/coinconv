HOMEDIR := $(CURDIR)
BIN_DIR := ${HOMEDIR}/bin

.PHONY: install-stringer
install-stringer:
	mkdir -p ${BIN_DIR}
	test -f ${BIN_DIR}/stringer || GOBIN=${BIN_DIR} go install golang.org/x/tools/cmd/stringer@latest

.PHONY: generate
generate: install-stringer
	PATH=$(BIN_DIR):$(PATH) go generate ./...
