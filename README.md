# coinconv

coinconv is a sample tool that converts currency amounts using [CoinMarketCap API](https://coinmarketcap.com/api/documentation/v1/)

## Usage

```shell
go build -o coinconv ./cmd/coinconv/main.go
./coinconv 48083 USD BTC
# or
go run cmd/coinconv/main.go 48083 USD BTC
```

## License

[MIT](http://opensource.org/licenses/MIT) © [Gaiaz Iusipov](https://gitlab.com/gaiaz)
