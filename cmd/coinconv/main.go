package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/shopspring/decimal"

	"gitlab.com/gaiaz/coinconv/internal/coinmarketcap"
	"gitlab.com/gaiaz/coinconv/internal/converter"
)

func main() {
	args := os.Args[1:]
	if argsLen := len(args); argsLen != 3 {
		log.Fatalf("exactly 3 args required, %d provided", argsLen)
	}

	amount, err := decimal.NewFromString(args[0])
	if err != nil {
		log.Fatalf("invalid amount arg %q", args[0])
	}

	if amount.LessThanOrEqual(decimal.Zero) {
		log.Fatalf("amount shold be a positive number %q provided", amount.String())
	}

	fromCurrency, ok := converter.ParseCurrency(args[1])
	if !ok {
		log.Fatalf("invalid first currency arg %q", args[1])
	}

	toCurrency, ok := converter.ParseCurrency(args[2])
	if !ok {
		log.Fatalf("invalid second currency arg %q", args[2])
	}

	httpClient := &http.Client{
		Timeout: 2 * time.Second,
	}

	coinMarketCap := coinmarketcap.New(httpClient)

	amountConverter := converter.New(coinMarketCap)

	result, err := amountConverter.ConvertAmount(context.Background(), amount, fromCurrency, toCurrency)
	if err != nil {
		log.Fatalf("amountConverter.ConvertAmount(): %v", err)
	}

	fmt.Println(result)
}
