package coinmarketcap

import (
	"errors"
)

type contract struct {
	Status struct {
		ErrorCode    int    `json:"error_code"`
		ErrorMessage string `json:"error_message"`
	} `json:"status"`
	Data map[string]struct {
		Symbol string `json:"symbol"`
		Quote  map[string]struct {
			Price float64 `json:"price"`
		} `json:"quote"`
	} `json:"data"`
}

func (c contract) validate() error {
	if c.Status.ErrorMessage != "" {
		return errors.New(c.Status.ErrorMessage)
	}
	if c.Status.ErrorCode != 0 {
		return errors.New("error code is not zero")
	}
	if len(c.Data) == 0 {
		return errors.New("empty data")
	}
	return nil
}

func (c contract) getPrice(cur string) float64 {
	for _, data := range c.Data {
		if quote, ok := data.Quote[cur]; ok {
			return quote.Price
		}
	}
	return 0
}
